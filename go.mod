module taler.net/taler-mailbox

go 1.18

require (
	git.gnunet.org/gnunet-go.git v0.1.28-0.20220717050634-369422be2512
	github.com/gorilla/mux v1.8.0
	gopkg.in/ini.v1 v1.66.6
	gorm.io/driver/postgres v1.3.8
	gorm.io/gorm v1.23.8
	taler.net/taler-go.git v0.0.0-20220719135513-36eb87bf37a3
)

require (
	github.com/bfix/gospel v1.2.15 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.12.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/pgx/v4 v4.16.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.14 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/text v0.3.7 // indirect
)
