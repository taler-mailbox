// This file is part of taler-mailbox, the Taler Mailbox implementation.
// Copyright (C) 2022 Martin Schanzenbach
//
// Taler-mailbox is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// Taler-mailbox is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"flag"
	"log"
	"net/http"

	mailbox "taler.net/taler-mailbox/pkg/rest"
)

var m mailbox.Mailbox

func handleRequests() {
	log.Fatal(http.ListenAndServe(m.Cfg.Section("mailbox").Key("bind_to").MustString("localhost:11000"), m.Router))
}

func main() {
	var cfgFlag = flag.String("c", "", "Configuration file to use")

	flag.Parse()
	cfgfile := "mailbox.conf"
	if len(*cfgFlag) != 0 {
		cfgfile = *cfgFlag
	}
	m := mailbox.Mailbox{}
	m.Initialize(cfgfile)
	handleRequests()
}
