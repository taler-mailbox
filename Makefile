all: gana server #cli

SCRIPT_TARGET:=$(shell dirname $(shell go list -f '{{.Target}}' ./cmd/mailbox-server))

gana:
	mkdir -p internal/gana
	git submodule update --init --recursive
	git submodule sync --recursive
	cd third_party/gana/gnu-taler-error-codes && make taler_error_codes.go
	cp third_party/gana/gnu-taler-error-codes/taler_error_codes.go internal/gana/

server:
	go build ./cmd/mailbox-server

#cli:
#	go build ./cmd/mailbox-cli

install: server #cli
	go install ./cmd/mailbox-server #&& go install ./cmd/mailbox-cli

.PHONY: all gana
